#' Function to create project skeleton
#'
#' @rdname create_project
#' @export

create_project <- function(){
    project.name <- readline("Enter project name: " )
    mesg <- paste0("Project destination is directory "
                  ,"in which to create the project folder. "
                  ,"Enter the path with Unix style path separators (forward slash)"
                  ,", even in Windows.")
    message(mesg)
    project.dest <- readline("Enter destination folder: " )
    project.folder <- create_folder(project.name, project.dest)
    od <- create_folder("original_data", project.folder)
    analysis <- create_folder("analysis", project.folder)
    sap.folder <- create_folder("SAP", project.folder)
    docs.folder <- create_folder("docs", project.folder)
    
    readme <- paste(project.folder
                  , "readme.md"
                  , sep = "/")

    rmd <- paste(analysis
               , "analysis.rmd"
               , sep = "/")

    sap <- paste(sap.folder
               , "sap.rmd"
               , sep = "/")

    sap.bib <- paste(sap.folder
               , "sap.bib"
               , sep = "/")

    before_body.tex <- paste(sap.folder
               , "before_body.tex"
               , sep = "/")
    
    templates <- generate_templates(project.name)
    
    if(file.create(readme))
        cat(templates[["readme"]], file = readme, sep = "\n")

    if(file.create(rmd))
        cat(templates[["rmd"]], file = rmd, sep = "\n")

    if(file.create(sap))
        cat(templates[["sap"]], file = sap, sep = "\n")

    if(file.create(sap.bib))
        cat(templates[["sap.bib"]], file = sap.bib, sep = "\n")

    if(file.create(before_body.tex))
        cat(templates[["before_body.tex"]], file = before_body.tex, sep = "\n")
    
    message("Successfully created the project.")
}




