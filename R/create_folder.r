#' Function to create a folder
#' @param name  name of the folder to be created
#' @param dest  dest of the folder to be created
#'
#' @examples
#' create_folder("test", "/tmp")
#' @rdname create_folder
#' @export

create_folder <- function(name, dest){
  exists <- TRUE
  folder <- paste(dest
                  , name
                  , sep = "/")
  if(dir.exists(dest)){
    message(paste("Folder", dest, "exists!"))
    if(file.access(dest, 2) != 0){
      stop(paste(dest,
                 "is not writeable"))
      exists <- FALSE
    } else {
      message("Creating project folder ", folder)
      dir.create(folder, recursive = TRUE)
    }
    
  } else {
    message("Creating project folder ", folder)
    exists <- dir.create(folder, recursive = TRUE)
  }
  if(! exists)
    stop("Please give a writable destination!")
  return(folder)
}




