---
title: Project cookie cutter
author: Shripad Sinari
date: <2020-03-17 Tue>
---

# Package

This package is based on single simple function to create a project skeleton.
The function `create_project` when called interactively asks for the project
`name` and `destination`. If the `destination` already exists and is writable
then it creates the project subfolder in that `destination` location. Else it
will attempt to create the path and the project folder recursively.

# Project structure

```
    project
    |	   readme
	|
	|
	-----  docs                    #place to store external docs
	|
	-----  original_data           #place to store data
	|
	-----  analysis                #place to store analysis
	|          analysis.rmd
	|
	-----  SAP                     #place to store statistical analysis plan
	           sap.rmd
			   sap.bib
			   before_body.tex
	
```

# Installation

```
   library("devtools")
   install_bitbucket("ssinari/prjcc.git", auth_user = <your-username>)
   
```

# Usage

```
    library(prjcc)
	# Invoke the function to create project
	> create_project()
	
```
